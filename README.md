# README #

## How do I get set up?

To start the application straightforward, do

> gradle bootRun

or to create a single .jar-file, do

> gradle build


## Usage

This version of the game "Scissor, Rock, Paper" lets you play against an AI.
It needs a "figure" and, optionally, a player name "player". 

### Example Json via POST: 

> {"figure":"PAPIER", "player":"Timo"}


### Example GET 

> ?player=Timo&figure=STEIN


## Rules

The game is playable with the "classic" rules at

> (POST) http://localhost:8080/playA

> (GET)  http://localhost:8080/playAGet

and with "advanced" rules (adding BRUNNEN) at

>(POST) http://localhost:8080/playB

> (GET)  http://localhost:8080/playBGet