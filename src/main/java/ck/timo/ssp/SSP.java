package ck.timo.ssp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


import ck.timo.ssp.converter.StringToFigureConverter;
import ck.timo.ssp.rules.Rules;
import ck.timo.ssp.rules.RulesA;
import ck.timo.ssp.rules.RulesB;

@SpringBootApplication
public class SSP extends WebMvcConfigurerAdapter {
	
	// Adding custom converter for parsing Figures out of GET-Requests.
	@Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToFigureConverter());
    }

    public static void main(String[] args) {
        SpringApplication.run(SSP.class, args);
    }

}