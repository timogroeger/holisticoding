package ck.timo.ssp.ai;

import ck.timo.ssp.message.Move;

/**
 * Superclass for implementing different types of AI, e.g. for keeping track of
 * humans' moves.
 * @author Timo
 *
 */
public abstract class AI {
	/**
	 * Queries a move from the AI
	 * @return Move
	 */
	abstract public Move getMove();
}
