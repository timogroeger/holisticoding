package ck.timo.ssp.ai;

import java.util.List;
import java.util.Random;

import ck.timo.ssp.message.Move;
import ck.timo.ssp.rules.Rules.Figure;

/**
 * A simple AI which simply selects a random figure from a given set.
 * @author Timo
 *
 */
public class SimpleRandomAI extends AI {
	/* The name of the AI */
	private final String AI_NAME = "AI";
	
	/* The figures available to the AI*/
	private List<Figure> availableFigures;
	
	public SimpleRandomAI(List<Figure> availableFigures) {
		this.availableFigures = availableFigures; 
	}
	
	public Move getMove() {
		   Random rnd = new Random();
		   int i = rnd.nextInt(this.availableFigures.size());
		   Figure f = this.availableFigures.get(i);
		   
		   return new Move(this.AI_NAME, f);
	}
	
}
