package ck.timo.ssp.controller;

import ck.timo.ssp.ai.AI;
import ck.timo.ssp.message.ErrorMessage;
import ck.timo.ssp.message.Message;
import ck.timo.ssp.message.Move;
import ck.timo.ssp.rules.Rules;

/**
 * Manages a game played by one player against an AI.
 * @author Timo
 *
 */
public abstract class AIGameController extends GameController {
	
	/* AI used for this game.*/
	AI ai;
	
	/**
	 * Initializes a game controller with a certain AI.
	 * @param r Rules to be used
	 */
	public AIGameController(Rules r, AI ai) {
		super(r);
		this.ai = ai;
	}	

	public Message processMove(Move m) {
		
		if (!this.rules.getAvailableFigures().contains(m.getFigure()))
			return new ErrorMessage(ErrorMessage.INVALID_FIGURE  
					+ this.rules.getAvailableFigures().toString());
		
		if (m.getPlayer() == null)
			m.setPlayer(Message.DEFAULT_PLAYER_NAME);
		
		return evalRound(m, ai.getMove());
	}
	

}
