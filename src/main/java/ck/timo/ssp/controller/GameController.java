package ck.timo.ssp.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.format.FormatterRegistry;

import ck.timo.ssp.converter.StringToFigureConverter;
import ck.timo.ssp.message.GameResultMessage;
import ck.timo.ssp.message.Message;
import ck.timo.ssp.message.Move;
import ck.timo.ssp.rules.*;
import ck.timo.ssp.rules.Rules.Figure;

/**
 * Provides evaluation logic for the game "Schere, Stein, Papier" and its derivates. 
 * @author Timo
 *
 */
abstract public class GameController {
	
	/* The rules of the played game */
	protected Rules rules;
	
	
	public GameController(Rules r) {
		this.rules = r;
	}
	
	
	/**
	 * Takes a move of a participant and processes it.
	 * @param m The move made
	 * @return Message to the Player
	 */
	public abstract Message processMove(Move m);
	
	/**
	 * Determines the winner of a round.
	 * @param m1 Move of Player 1
	 * @param m2 Move of Player 2
	 * @param r The rules this round follows
	 * @return Result of the round.
	 */
	protected GameResultMessage evalRound(Move m1, Move m2) {
		boolean draw = false;
		String winner = "";
		
		// Find the winner
		int res = this.rules.getEvaluationRules().get(m1.getFigure()).get(m2.getFigure());
		
		// Prepare result
		if (res == 0)
			draw = true;
		else if (res == 1)
			winner = m1.getPlayer();
		else 
			winner = m2.getPlayer();
		
		// Combine moves to list
		List<Move> moveList = new ArrayList<>(Arrays.asList(m1,m2));

		return new GameResultMessage(moveList, draw, winner);
	}	   
}
