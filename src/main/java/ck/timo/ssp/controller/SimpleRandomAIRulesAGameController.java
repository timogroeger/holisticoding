package ck.timo.ssp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ck.timo.ssp.ai.SimpleRandomAI;
import ck.timo.ssp.message.Message;
import ck.timo.ssp.message.Move;
import ck.timo.ssp.rules.Rules;
import ck.timo.ssp.rules.Rules.Figure;
import ck.timo.ssp.rules.RulesA;

/**
 * Implements a concrete controller for playing against a simple AI using the "classic"
 * set of figures.
 * @author Timo
 *
 */
@RestController
public class SimpleRandomAIRulesAGameController extends AIGameController {
	
	public SimpleRandomAIRulesAGameController() {
		super(RulesA.getInstance(), new SimpleRandomAI(RulesA.getInstance().getAvailableFigures()));
	}
	
    @RequestMapping("/playA")
    public Message play(@RequestBody Move m){
    	return processMove(m);
    }
    
    // This mapping is the get-equivalent to the post-mapping above    
    @RequestMapping("/playAGet")
    public Message playGet(@RequestParam(value="player", defaultValue="Human") String player, @RequestParam(value="figure", defaultValue="") Figure figure) {
        return processMove(new Move(player, figure));
    }

}
