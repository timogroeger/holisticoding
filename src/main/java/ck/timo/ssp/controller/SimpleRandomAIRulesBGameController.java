package ck.timo.ssp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ck.timo.ssp.ai.AI;
import ck.timo.ssp.ai.SimpleRandomAI;
import ck.timo.ssp.message.Message;
import ck.timo.ssp.message.Move;
import ck.timo.ssp.rules.Rules;
import ck.timo.ssp.rules.RulesA;
import ck.timo.ssp.rules.RulesB;
import ck.timo.ssp.rules.Rules.Figure;

/**
 * Implements a concrete controller for playing against a simple AI using an extended set of figures.
 * @author Timo
 *
 */
@RestController
public class SimpleRandomAIRulesBGameController extends AIGameController {

	public SimpleRandomAIRulesBGameController() {
		super(RulesB.getInstance(), new SimpleRandomAI(RulesB.getInstance().getAvailableFigures()));
		}
	
    @RequestMapping("/playB")
    public Message play(@RequestBody Move m){
        return processMove(m);
    }
    
    // This mapping is the get-equivalent to the post-mapping above    
    @RequestMapping("/playBGet")
    public Message playGet(@RequestParam(value="player", defaultValue="Human") String player, @RequestParam(value="figure", defaultValue="") Figure figure) {
        return processMove(new Move(player, figure));
    }


}
