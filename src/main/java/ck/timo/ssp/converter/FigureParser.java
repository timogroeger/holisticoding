package ck.timo.ssp.converter;

import ck.timo.ssp.rules.Rules.Figure;

public class FigureParser {
	
	public static Figure parseFigureFromString(String figure) {
		Figure parsedFigure = null;
		String tmp = figure.trim().toUpperCase(); // cleanse input
	
		try { 
			
			parsedFigure = Figure.valueOf(tmp); // try to find Figure equivalent
			
		} catch(IllegalArgumentException e) {
			
			parsedFigure = null;
		} 
		
		return parsedFigure;
	}
}
