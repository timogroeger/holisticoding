package ck.timo.ssp.converter;

import org.springframework.core.convert.converter.Converter;

import ck.timo.ssp.rules.*;
import ck.timo.ssp.rules.Rules.Figure;

public class StringToFigureConverter implements Converter<String, Rules.Figure> {

	/**
	 * Parses a Figure from a given string. Is used for parsing Figures out 
	 * of GET-Requests.
	 * @param figure The String from which a Figure is being tried to parse.
	 * @return The respective Figure if f was valid, else null.
	 */
    public Figure convert (String figure) {
    	return FigureParser.parseFigureFromString(figure);
    }
	
}


