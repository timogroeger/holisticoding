package ck.timo.ssp.message;

/**
 * This class represents an occurred error.
 * @author Timo
 *
 */
public class ErrorMessage extends Message {
	
	/* Standard message for invalid figures */
	public static final String INVALID_FIGURE = "Invalid figure used for your move. Please use one of these: ";
	
	/* A message specifying the error. */
	private String errorMessage;
	
	/**
	 * Creates an error with a certain message.
	 * @param errorMessage Message
	 */
	public ErrorMessage (String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
