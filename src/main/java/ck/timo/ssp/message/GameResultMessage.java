package ck.timo.ssp.message;

import java.util.List;


/**
 * This class describes the prerequisite and the outcome of the game played.
 * @author Timo
 *
 */
public class GameResultMessage extends Message {

	/* Shows the moves made in this game. */
	private List<Move> movesMade;
	
	/* Indicates a draw game */
	private boolean isDraw;
	
	/* Carries the winners' name. */
	private String winner;

	/**
	 * Creates a message to represent the outcome of a game.
	 * @param movesMade List of moves made by the players
	 * @param isDraw Indicates a draw game
	 * @param winner The winner of the game
	 */
	public GameResultMessage(List<Move> movesMade, boolean isDraw, String winner){
		this.movesMade = movesMade;
		this.isDraw = isDraw;
		this.winner = winner;
	}

	public List<Move> getMovesMade() {
		return movesMade;
	}

	public boolean isDraw() {
		return isDraw;
	}

	public String getWinner() {
		return winner;
	}
	
	public void setMovesMade(List<Move> movesMade) {
		this.movesMade = movesMade;
	}
	
	public void setIsDraw (boolean isDraw) {
		this.isDraw = isDraw;
	}
	
	public void setWinner (String winner) {
		this.winner = winner;
	}

}
