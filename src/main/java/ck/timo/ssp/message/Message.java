package ck.timo.ssp.message;

/** 
 * This class represents a message sent between participants of the game.
 * @author Timo
 *
 */
public abstract class Message {
	
	public static String DEFAULT_PLAYER_NAME = "Human";

}
