package ck.timo.ssp.message;

import ck.timo.ssp.rules.Rules.Figure;

/**
 * This class represents a move made by a player.
 * @author Timo
 *
 */
public class Move extends Message {

	/* The player executing this move. */
	private String player;
	
	/* The shown figure in this move. */
	private Figure figure;

	
	public Move (String player, Figure figure) {
		this.player = player;
		this.figure = figure;
	}
	
	public Move() { }
	
	public String getPlayer() {
		return player;
	}

	public Figure getFigure() {
		return figure;
	}

	public void setPlayer(String p){
		this.player = p;
	}
	
	public void setFigure(Figure f){
		this.figure = f;
	}
	
}
