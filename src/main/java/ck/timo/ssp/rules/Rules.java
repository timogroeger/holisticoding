package ck.timo.ssp.rules;


import java.util.List;
import java.util.Map;

import ck.timo.ssp.converter.FigureParser;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * This class represents a set of rules to play the game.
 * @author Timo
 *
 */
public abstract class Rules {
	
	/* Publicly specifies all available figures to the game. */
	public static enum Figure{SCHERE, STEIN, PAPIER, BRUNNEN;
		
	/**
	 * Error handling if the user sends an invalid figure. Spring/Jackson uses this method to cast
	 * from String to Figure.
	 * @param figure String which is to cast to a figure
	 * @return Figure if a valid figure was entered, else null.
	 */
		@JsonCreator
		public static Figure fromText(String figure) {
			return FigureParser.parseFigureFromString(figure);
	}
	};
	
	/* Available figures for a concrete set of rules. */
	protected List<Figure> availableFigures;
	
	/* Evaluation scheme (i.e. what-wins-against-what) for a concrete set of rules. */
	protected Map<Figure, Map<Figure, Integer>> evaluationScheme;
	
	public Rules() {
		initAvailableFigures();
		initEvaluationScheme();
	}
	
	/**
	 * Specifies the set of available figures for a concrete set of rules.
	 */
	abstract protected void initAvailableFigures();
	
	/**
	 * Specifies the evaluation scheme for a concrete set of rules.
	 */
	abstract protected void initEvaluationScheme();

	/**
	 * Returns the available figures for a concrete set of rules.
	 * @return Available figures.
	 */
	public List<Figure> getAvailableFigures() {
		return this.availableFigures;
	}
	
	/**
	 * Returns the evaluation scheme (i.e. what-wins-against-what) for a concrete set of rules. 
	 * @return Evaluation scheme
	 */
	public Map<Figure, Map<Figure, Integer>> getEvaluationRules() {
		return this.evaluationScheme;
	}	
	
}
