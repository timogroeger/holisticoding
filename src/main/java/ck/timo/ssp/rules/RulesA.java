package ck.timo.ssp.rules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * Specifies the conditions for the "classic" Schere-Stein-Papier game.
 * @author timo
 *
 */
public class RulesA extends Rules {

	/* Instance of this set of rules */
	private static RulesA instance;
	
	private RulesA() { }
	
	/**
	 * Specifies the set of available figures for the classic game type, which is SCHERE, STEIN, PAPIER.
	 */
	protected void initAvailableFigures() {
		this.availableFigures = new ArrayList<>(Arrays.asList(
				Figure.SCHERE,
				Figure.STEIN,
				Figure.PAPIER));
	}
	
	/**
	 * Specifies the evaluation scheme for the classic game type, which is
	 * 
	 *   p1 / p2   Schere   Stein   Papier  
	 *	Schere    0        -1      1       
	 *	Stein     1        0       -1      
  	 *	Papier    -1       1       0   
	 */
	protected void initEvaluationScheme() {
		this.evaluationScheme = new HashMap<>();
		
		Map<Figure, Integer> schereMap = new HashMap<>();
		schereMap.put(Figure.SCHERE, 0);
		schereMap.put(Figure.STEIN, -1);
		schereMap.put(Figure.PAPIER, 1);

		Map<Figure, Integer> steinMap = new HashMap<>();
		steinMap.put(Figure.SCHERE, 1);
		steinMap.put(Figure.STEIN, 0);
		steinMap.put(Figure.PAPIER, -1);
		
		Map<Figure, Integer> papierMap = new HashMap<>();
		papierMap.put(Figure.SCHERE, -1);
		papierMap.put(Figure.STEIN, 1);
		papierMap.put(Figure.PAPIER, 0);
		
		this.evaluationScheme.put(Figure.SCHERE, schereMap);
		this.evaluationScheme.put(Figure.STEIN, steinMap);
		this.evaluationScheme.put(Figure.PAPIER, papierMap);
	}
	
	/**
	 * Returns the static instance of this concrete set of rules.
	 * @return Rules
	 */
	public static RulesA getInstance() {
		
		if (RulesA.instance == null)
				RulesA.instance = new RulesA();
		
		return instance;
	}
}
