package ck.timo.ssp.controller;

import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ck.timo.ssp.controller.AIGameController;
import ck.timo.ssp.controller.SimpleRandomAIRulesAGameController;
import ck.timo.ssp.controller.SimpleRandomAIRulesBGameController;
import ck.timo.ssp.message.GameResultMessage;
import ck.timo.ssp.message.Move;
import ck.timo.ssp.rules.Rules;
import ck.timo.ssp.rules.Rules.Figure;
import ck.timo.ssp.rules.RulesA;
import ck.timo.ssp.rules.RulesB;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AIControllerTest {
 
	static Rules rulesA;
	static Rules rulesB;
	
	static Move p1SchereMove;
	static Move p1SteinMove;
	static Move p1PapierMove;
	static Move p1BrunnenMove;
	
	static Move p2SchereMove;
	static Move p2SteinMove;
	static Move p2PapierMove;
	static Move p2BrunnenMove;
	
	@BeforeClass
	public static void init() {
		rulesA = RulesA.getInstance();
		rulesB = RulesB.getInstance();
		
		p1SchereMove = new Move("P1", Figure.SCHERE);
		p1SteinMove = new Move("P1", Figure.STEIN);
		p1PapierMove = new Move("P1", Figure.PAPIER);
		p1BrunnenMove = new Move("P1", Figure.BRUNNEN);
		
		p2SchereMove = new Move("P2", Figure.SCHERE);
		p2SteinMove = new Move("P2", Figure.STEIN);
		p2PapierMove = new Move("P2", Figure.PAPIER);
		p2BrunnenMove = new Move("P2", Figure.BRUNNEN);
	}
	
   
    @Test
    public void rulesAOnePlayerAlwaysSchere() {
    	SimpleRandomAIRulesAGameController c = new SimpleRandomAIRulesAGameController();
        Assert.assertEquals(c.evalRound(p1SchereMove, p2SchereMove).getWinner(), "");
        Assert.assertEquals(c.evalRound(p1SchereMove, p2SteinMove).getWinner(), "P2");
        Assert.assertEquals(c.evalRound(p1SchereMove, p2PapierMove).getWinner(), "P1");
    }
    
    @Test
    public void rulesAOnePlayerAlwaysStein() {
    	SimpleRandomAIRulesAGameController c = new SimpleRandomAIRulesAGameController();
        Assert.assertEquals(c.evalRound(p1SteinMove, p2SchereMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1SteinMove, p2SteinMove).getWinner(), "");
        Assert.assertEquals(c.evalRound(p1SteinMove, p2PapierMove).getWinner(), "P2");
    }
    
    @Test
    public void rulesAOnePlayerAlwaysPapier() {
    	SimpleRandomAIRulesAGameController c = new SimpleRandomAIRulesAGameController();
        Assert.assertEquals(c.evalRound(p1PapierMove, p2SchereMove).getWinner(), "P2");
        Assert.assertEquals(c.evalRound(p1PapierMove, p2SteinMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1PapierMove, p2PapierMove).getWinner(), "");
    }
    
    @Test
    public void rulesBOnePlayerAlwaysSchere() {
    	SimpleRandomAIRulesBGameController c = new SimpleRandomAIRulesBGameController();
        Assert.assertEquals(c.evalRound(p1SchereMove, p2SchereMove).getWinner(), "");
        Assert.assertEquals(c.evalRound(p1SchereMove, p2SteinMove).getWinner(), "P2");
        Assert.assertEquals(c.evalRound(p1SchereMove, p2PapierMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1SchereMove, p2BrunnenMove).getWinner(), "P2");
    }
    
    @Test
    public void rulesBOnePlayerAlwaysStein() {
    	SimpleRandomAIRulesBGameController c = new SimpleRandomAIRulesBGameController();
        Assert.assertEquals(c.evalRound(p1SteinMove, p2SchereMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1SteinMove, p2SteinMove).getWinner(), "");
        Assert.assertEquals(c.evalRound(p1SteinMove, p2PapierMove).getWinner(), "P2");
        Assert.assertEquals(c.evalRound(p1SteinMove, p2BrunnenMove).getWinner(), "P2");
    }
    
    @Test
    public void rulesBOnePlayerAlwaysPapier() {
    	SimpleRandomAIRulesBGameController c = new SimpleRandomAIRulesBGameController();
        Assert.assertEquals(c.evalRound(p1PapierMove, p2SchereMove).getWinner(), "P2");
        Assert.assertEquals(c.evalRound(p1PapierMove, p2SteinMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1PapierMove, p2PapierMove).getWinner(), "");
        Assert.assertEquals(c.evalRound(p1PapierMove, p2BrunnenMove).getWinner(), "P1");
    }
    
    @Test
    public void rulesBOnePlayerAlwaysBrunnen() {
    	SimpleRandomAIRulesBGameController c = new SimpleRandomAIRulesBGameController();
        Assert.assertEquals(c.evalRound(p1BrunnenMove, p2SchereMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1BrunnenMove, p2SteinMove).getWinner(), "P1");
        Assert.assertEquals(c.evalRound(p1BrunnenMove, p2PapierMove).getWinner(), "P2");
        Assert.assertEquals(c.evalRound(p1BrunnenMove, p2BrunnenMove).getWinner(), "");
    }
}