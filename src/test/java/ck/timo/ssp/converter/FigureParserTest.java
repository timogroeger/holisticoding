package ck.timo.ssp.converter;

import org.junit.Assert;
import org.junit.Test;

import ck.timo.ssp.rules.Rules.Figure;

public class FigureParserTest {

	
	@Test
    public void parseKnownFigures() {
        Assert.assertEquals(FigureParser.parseFigureFromString("SCHERE"), Figure.SCHERE);
        Assert.assertEquals(FigureParser.parseFigureFromString("STEIN"), Figure.STEIN);
        Assert.assertEquals(FigureParser.parseFigureFromString("PAPIER"), Figure.PAPIER);
        Assert.assertEquals(FigureParser.parseFigureFromString("BRUNNEN"), Figure.BRUNNEN);    
    }
	
	@Test
    public void parseKnownFiguresAlteringCapitalization() {
        Assert.assertEquals(FigureParser.parseFigureFromString("schERE"), Figure.SCHERE);
        Assert.assertEquals(FigureParser.parseFigureFromString("stEin"), Figure.STEIN);
        Assert.assertEquals(FigureParser.parseFigureFromString("papier"), Figure.PAPIER);
        Assert.assertEquals(FigureParser.parseFigureFromString("BrUnNeN"), Figure.BRUNNEN);    
    }
	
	@Test
    public void parseUnknownFigures() {
        Assert.assertEquals(FigureParser.parseFigureFromString("tisch"), null);
        Assert.assertEquals(FigureParser.parseFigureFromString(""), null);
        Assert.assertEquals(FigureParser.parseFigureFromString("scere"), null);  
    }
	
}
