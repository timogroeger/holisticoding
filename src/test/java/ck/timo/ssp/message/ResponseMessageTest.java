package ck.timo.ssp.message;

import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ResponseMessageTest {
	
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void playerIsInMoveHistory() throws Exception {
    	  	
		mockMvc.perform(post("/playA").contentType(MediaType.APPLICATION_JSON).content("{\"player\":\"P1\",\"figure\":\"PAPIER\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString("{\"player\":\"P1\",\"figure\":\"PAPIER\"}")));
		
		mockMvc.perform(post("/playB").contentType(MediaType.APPLICATION_JSON).content("{\"player\":\"P1\",\"figure\":\"BRUNNEN\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString("{\"player\":\"P1\",\"figure\":\"BRUNNEN\"}")));

    }
    
    @Test
    public void playerSentInvalidFigure() throws Exception {
    	  	
		mockMvc.perform(post("/playA").contentType(MediaType.APPLICATION_JSON).content("{\"player\":\"P1\",\"figure\":\"TISCH\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString(ErrorMessage.INVALID_FIGURE)));
		
		mockMvc.perform(post("/playB").contentType(MediaType.APPLICATION_JSON).content("{\"player\":\"P1\",\"figure\":\"TISCH\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString(ErrorMessage.INVALID_FIGURE)));

    }
    
    @Test
    public void playerSentNoFigure() throws Exception {
    	  	
		mockMvc.perform(post("/playA").contentType(MediaType.APPLICATION_JSON).content("{\"player\":\"P1\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString(ErrorMessage.INVALID_FIGURE)));
		
		mockMvc.perform(post("/playB").contentType(MediaType.APPLICATION_JSON).content("{\"player\":\"P1\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString(ErrorMessage.INVALID_FIGURE)));

    }
    
    @Test
    public void playerSentNoNameExpectingStandardName() throws Exception {
    	  	
		mockMvc.perform(post("/playA").contentType(MediaType.APPLICATION_JSON).content("{\"figure\":\"STEIN\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString("{\"player\":\""+ Move.DEFAULT_PLAYER_NAME +"\",\"figure\":\"STEIN\"}")));
		
		mockMvc.perform(post("/playB").contentType(MediaType.APPLICATION_JSON).content("{\"figure\":\"STEIN\"}"))
		.andExpect(status().isOk()).andExpect(content().string(containsString("{\"player\":\""+ Move.DEFAULT_PLAYER_NAME +"\",\"figure\":\"STEIN\"}")));

    }
}